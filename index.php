<?php
require 'vendor/autoload.php';


Flight::register('MainView', 'Acme\Views\MainView');

Flight::route('/', ['Acme\Controllers\Main', 'main']);

Flight::route('/test', ['Acme\Controllers\Test', 'test']);

Flight::start();
