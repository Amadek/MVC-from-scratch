$(document).ready(function() {

    function getData(url, obj) {
        if (!$(obj).hasClass('active'))
        {
            $('.nav-tabs a').removeClass('active')
            $(obj).addClass('active')
            $.ajax({
                url: url,
                success: function(result) {
                    $('#content').html(result).hide().fadeIn()
                },
                error: function functionName(result) {
                    $('.alert-warning').hide().removeClass('d-none').fadeIn()
                    $('.alert-warning').html(result.status + " " + result.statusText)
                    console.log(result.responseText)
                }
            })
        }
    }

    $('#UserButton').click(function() {
        getData("/?tab=users", this)
    })

    $('#MemeButton').click(function() {
        getData("/?tab=memes", this)
    })

    $('#ReplyButton').click(function() {
        getData("/?tab=replies", this)
    })
})
