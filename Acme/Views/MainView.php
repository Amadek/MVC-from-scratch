<?php
namespace Acme\Views;

class MainView
{
    private $Flight;

    public function __construct()
    {
        $loader = new \Twig_Loader_Filesystem('Acme/Templates');
        $this->Flight = new \Twig_Environment($loader);
    }

    public function main(array $memes)
    {
        return $this->Flight->render('home.html.twig', [
            'memes' => $memes
        ]);
    }

    public function memes(array $memes)
    {
        return $this->Flight->render('home_memes.html.twig', [
            'memes' => $memes
        ]);
    }

    public function users(array $users)
    {
        return $this->Flight->render('home_users.html.twig', [
            'users' => $users
        ]);
    }

    public function replies(array $replies)
    {
        return $this->Flight->render('home_replies.html.twig', [
            'replies' => $replies
        ]);
    }
}
