<?php
namespace Acme\Controllers;
use Acme\Models\User;

class Test
{
    public function test()
    {
        $users = json_encode(User::getAll());
        if (isset($_GET['q'])) {
            echo $users;
        } else {
            $loader = new \Twig_Loader_Filesystem('Acme/Templates');
            $flight = new \Twig_Environment($loader);
            echo $flight->render('test.html.twig');
        }
    }
}
