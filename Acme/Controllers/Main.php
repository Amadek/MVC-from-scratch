<?php
namespace Acme\Controllers;
use Acme\Models\User;
use Acme\Models\Meme;
use Acme\Models\Reply;
use Acme\Views\Views;
use Acme\Tools\PDOConnector;

class Main
{
    public static function main()
    {
        $tab = isset($_GET['tab']) ? $_GET['tab'] : null;
        switch ($tab) {
            // For AJAX.
            case "memes":
                echo \Flight::MainView()->memes(Meme::getAll());
                break;
            case 'users':
                echo \Flight::MainView()->users(User::getAll());
                break;
            case 'replies':
                echo \Flight::MainView()->replies(Reply::getAll());
                break;
            // For normal request.
            default:
                echo \Flight::MainView()->main(Meme::getAll());
                break;
        }
    }
}
