<?php
namespace Acme\Tools;

class PDOConnector
{
    private static function connect()
    {
        try {
            $db = new \PDO('mysql:host=localhost;dbname=new_mem_site', 'root', '');
            $db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            return $db;
        } catch(\PDOException $e) {
            die($e->getMessage());
        }
    }

    public static function doOnConnect(/*function($db)*/ $func)
    {
        $db = self::connect();
        $result = $func($db);
        $db = null;
        return $result;
    }
}
