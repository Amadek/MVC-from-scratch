<?php
namespace Acme\Tools;

class Validator
{
    public static function isArrayIntegrated(array $keys, array $the_array)
    {
        foreach ($keys as $key) {
            if (!array_key_exists($key, $the_array)) {
                return false;
            }
        }
        return true;
    }
}
