<?php
namespace Acme\Models;
use Acme\Tools\PDOConnector;
use Acme\Models\Meme;

class User implements ISelectable
{
    public $id;
    public $name;
    public $isAdmin;
    public $created;
    public $memes = [];

    public function __construct(string $name)
    {
        $result = PDOConnector::doOnConnect(function($db) use ($name) {
            $sql = 'SELECT * FROM users WHERE name = :name';
            $stmt = $db->prepare($sql);
            $stmt->execute([':name' => $name]);
            $result = $stmt->fetch(\PDO::FETCH_ASSOC);

            if (!empty($result)) {
                return $result;
            } else {
                throw new UserException("Nie ma takiego usera");
            }
        });

        $this->id = $result['id'];
        $this->name = $result['name'];
        $this->isAdmin = $result['isAdmin'];
        $this->created = $result['created'];
        $this->getMemes();
    }

    private function getMemes()
    {
        $id = $this->id;
        $result = PDOConnector::doOnConnect(function($db) use ($id) {
            $sql = 'SELECT * FROM memes WHERE owner = :owner';
            $stmt = $db->prepare($sql);
            $stmt->execute([':owner' => $id]);
            $result = $stmt->fetchAll();
            return $result;
        });

        $this->memes = $result;
    }

    public static function getAll()
    {
        $users = PDOConnector::doOnConnect(function($db) {
            $sql = 'SELECT * FROM users';
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        });

        return $users;
    }

    public static function removeSpec(array $ids)
    {
        PDOConnector::doOnConnect(function($db) use ($ids) {
            $sql = 'DELETE FROM us where id = :id';
            $stmt = $db->prepare($sql);
            foreach ($ids as $id) {
                $stmt->execute([':id' => $id]);
            }
        });
    }
}
