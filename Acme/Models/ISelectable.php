<?php
namespace Acme\Models;

interface ISelectable
{
    static function getAll();

    static function removeSpec(array $ids);
}
