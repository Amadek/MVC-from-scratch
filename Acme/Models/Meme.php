<?php
namespace Acme\Models;
use Acme\Tools\PDOConnector;
use Acme\Tools\Validator;

class Meme implements ISelectable
{
    public static function getAll()
    {
        return PDOConnector::doOnConnect(function($db) {
            $sql = 'SELECT * FROM memes';
            $stmt = $db->prepare($sql);
            $stmt->execute();

            $table = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $table;
        });
    }

    public static function removeSpec(array $ids)
    {
        PDOConnector::doOnConnect(function($db) use ($ids) {
            $sql = 'DELETE FROM memes where id = :id';
            $stmt = $db->prepare($sql);
            foreach ($ids as $id) {
                $stmt->execute([':id' => $id]);
            }
        });
    }
}
