<?php
namespace Acme\Models;
use Acme\Tools\PDOConnector;

class Reply implements ISelectable
{
    public static function getAll()
    {
        return PDOConnector::doOnConnect(function($db) {
            $sql = 'SELECT * FROM replies';
            $stmt = $db->prepare($sql);
            $stmt->execute();

            $table = $stmt->fetchAll();
            return $table;
        });
    }

    public static function removeSpec(array $ids)
    {
        PDOConnector::doOnConnect(function($db) use ($ids) {
            $sql = 'DELETE FROM replies where id = :id';
            $stmt = $db->prepare($sql);
            foreach ($ids as $id) {
                $stmt->execute([':id' => $id]);
            }
        });
    }
}
