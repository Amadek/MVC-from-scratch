<?php
namespace Acme\Tools\Exceptions;

class MyException extends \Exception
{
    public function __construct(string $message)
    {
        parent::__construct(get_class($this) . ": " . $message);
    }
}
